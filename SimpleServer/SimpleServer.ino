#include <ESP8266WiFi.h> //for the WiFi connection
#include "ESP8266WebServer.h" //for the Server
ESP8266WebServer server(80);

// Set WiFi credentials
#define WIFI_SSID "YOURSSID"
#define WIFI_PASS "YOURPASSWORD"

// Set AP credentials
#define AP_SSID "YOUR_DESIRED_AP_NAME" //how your AP will be visible in your wifi list
#define AP_PASS "YOUR_DESIRED_AP_PW"

//Extra's. these are optional
#define AP_CHANNEL 1   //choose the channel
#define AP_HIDDEN false //SSID broadcasted or not
#define AP_MAX_CON 4 // max nr of connections to the AP

uint8_t macAddr[6];

//for static IP (all not necessary if you choose DHCP)
// Set IP addresses
IPAddress local_IP(192, 168, 1, 70);
IPAddress gateway(192, 168, 1, 1);
IPAddress subnet(255, 255, 255, 0);
//change the default IP address for the AP (normally 192.168.4.1)
IPAddress AP_IP(192,168,4,5);


void setup() {
  // Setup serial port
  Serial.begin(115200);

  //begin Acces Point
  WiFi.mode(WIFI_AP_STA);
  //WiFi.mode(WIFI_STA);// for just Station connection
  //WiFi.mode(WIFI_AP);//for just AP connection

  //WiFi.softAP(AP_SSID, AP_PASS); //this makes your Acces point connection
  WiFi.softAPConfig(AP_IP, AP_IP, subnet);// this is optional
  WiFi.softAP(AP_SSID, AP_PASS, AP_CHANNEL, AP_HIDDEN, AP_MAX_CON);

  // Begin WiFi
  WiFi.config(local_IP, gateway, subnet);// optional, for  static connection
  WiFi.begin(WIFI_SSID, WIFI_PASS);// this makes your STA connection


  // Loop continuously while WiFi is not connected
  while (WiFi.status() != WL_CONNECTED)
  {
    delay(100);
    Serial.print(".");
  }

  // Connected to WiFi
  WiFi.setAutoReconnect(true); // For automatic reconnect
  WiFi.persistent(true); // Ditto
  Serial.println("Connected");
  Serial.print("Network IP address: ");
  Serial.println(WiFi.localIP());
  Serial.print("IP address for network ");
  Serial.print(WIFI_SSID);
  Serial.print(" : ");
  Serial.println(WiFi.localIP());
  Serial.print("IP address for network ");
  Serial.print(AP_SSID);
  Serial.print(" : ");
  Serial.println(WiFi.softAPIP());
  WiFi.softAPmacAddress(macAddr);
  Serial.printf("MAC address = %02x:%02x:%02x:%02x:%02x:%02x\n", macAddr[0], macAddr[1], macAddr[2], macAddr[3], macAddr[4], macAddr[5]);
  Serial.println("");
  //----------------
  server.on("/other", []() {   //Define the handling function for the path

    server.send(200, "text / plain", "Other URL");

  });

  server.on("/", handleRootPath);    //Associate the handler function to the path
  server.begin();                    //Start the server
  Serial.println("Server listening");
}

void loop() {
  server.handleClient();         //Handling of incoming requests
  // put your main code here, to run repeatedly:
  Serial.print("number of clients connected:");
  Serial.println(WiFi.softAPgetStationNum());
delay(3000);
  
}

// for a simple proof of context it would have been enough to just do server.send(200,"text/plain", "Hello World");
// but I added some html to make the content adapt to the size of the viewing device, so it would be properly visible on a phone as well
void handleRootPath() {            //Handler for the rooth path
  server.send(200, "text/html", "<!DOCTYPE HTML><html><head><meta name='viewport' content='width=device-width,initial-scale=1'></head><body>Hello world</body></html>");

}
